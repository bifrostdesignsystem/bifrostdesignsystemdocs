import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';
import Link from '@docusaurus/Link';

const FeatureList = [
  {
    title: 'Canonical  ',
    Svg: require('../../static/svg/undraw_real-time_sync.svg').default,
    description: (
      <>
        Bifröst collects Design Decisions and Patterns into a single source of truth by dilivering ad Design Tokens library called <Link to="/tokens/intro"><code>Bifröst Tokens</code></Link>
      </>
    ),
  },
  {
    title: 'Agnostic',
    Svg: require('../../static/svg/undraw_design_components.svg').default,
    description: (
      <>
        Bifröst has an agnostic approach to design specifications and guidelines for component based UI via <Link to="/components/intro"><code>Bifröst Components</code></Link>
      </>
    ),
  },
  {
    title: 'Guidance',
    Svg: require('../../static/svg/undraw_abstract.svg').default,
    description: (
      <>
        Bifröst provide guidelines and insights into company visions and standards <Link to="docs/intro"><code>Bifröst Docs</code></Link>
      </>
    ),
  },
];

function Feature({ Svg, title, description }) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
