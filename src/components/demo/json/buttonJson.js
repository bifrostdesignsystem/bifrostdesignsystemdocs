import React from 'react'
import styled from 'styled-components'
import BT from '@bifrost-ds/bifrosttokens'

// use json flat format of Bifröst Tokens
const t = BT.json.flat;

const Button = styled.button`

    /* ! Using Bifröst Tokens CSS format
    /* Bacgkorund Color */
    background-color: ${t['bt-component-button-primary-background-color']};
    /* Text */
    color: ${t['bt-component-button-primary-text-color']};
    font-size: ${t['bt-component-button-primary-font-size']};
    /* Border  */
    border-color: ${t['bt-component-button-primary-border-color']};
    border-radius: ${t['bt-component-button-primary-border-radius']};
    border-width: ${t['bt-component-button-primary-border-width']};

    /* Padding */
    padding: ${t['bt-component-button-primary-padding-vertical']} ${t['bt-component-button-primary-padding-horizontal']}; 

    /* Static */
    font-family: 'Bitter', serif;
    border-style: solid;
    cursor: pointer;
`


const ButtonJson = ({children}) => {
    return (
        <>
            <Button>{children}</Button>
        </>
    )
}

export default ButtonJson;
