import React from 'react'
import styles from './Button.module.css';

const ButtonCSS = ({children}) => {
    return (
        <>
            <button className={styles.button}>{children}</button>
        </>
    )
}

export default ButtonCSS;
