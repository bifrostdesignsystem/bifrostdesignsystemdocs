import React from 'react'
import styled from "styled-components";
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import { FaRegEdit as EditIcon } from 'react-icons/fa';

const Table = styled.table`
    border: 1px solid var(--bt-color-neutral-grey-b-20);
    background-color: transparent;
    height: 400px;
    overflow-y: scroll;
    overflow-x: hidden;

    & tr:nth-child(2n){
        background-color: transparent;
    }

    
`
const Tr = styled.tr`
    width: 100%;
    background-color: transparent;
    border-bottom: 1px solid var(--bt-color-neutral-grey-b-20);

`
const Th = styled.th`
    width: 100%;
    text-align: left;
    border: none;
    border-bottom: 1px solid var(--bt-color-neutral-grey-b-20);
    background-color: var(--ifm-table-stripe-background);
`
const Td = styled.td`
    background-color: transparent;
    border: none;

`


const TokensList = (props) => {
   const {siteConfig} = useDocusaurusContext();
   const {tokensArray} = props;

   const bifrostTokensRepositoryBaseUrl = siteConfig.customFields.bifrostTokensRepository.baseUrl;

   const tokenList = tokensArray.map(t => {
       const editTokenUrl = `${bifrostTokensRepositoryBaseUrl}/${t.filePath}`;

        return (
            <Tr>
                {/* Token: */}
                <Td>
                    <div><b>Name: </b>{t.name}</div>
                    {t.comment && <div><b>Comment: </b>{t.comment}</div>}
                    {t.description && <div><b>Description: </b>{t.description}</div>}
                </Td>
                {/* Example */}
                <Td>
                    {t.attributes.category === 'color' &&  <div style={{width: "100%", height: "12.5px", backgroundColor: `${t.value}`}}></div>}
                    <div>{t.value}</div>
                </Td>
                {/* Edit */}
                <Td>
                    <a href={editTokenUrl} target="_blank"><EditIcon /> </a>
                </Td>
            </Tr>
        )
    })

    return (
        <Table>
            <Tr>
                <Th>Token</Th>
                <Th>Example</Th>
                <Th>Edit</Th>
            </Tr>
            {tokenList}
        </Table>
    )
}

export default TokensList
