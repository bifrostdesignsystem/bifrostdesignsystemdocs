/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Bifröst',
  tagline: 'Bifrost provides Design Decision, Design Tokens, and Component -documentation to help product teams work more efficiently, and to make applications more cohesive.',
  url: 'https://your-docusaurus-test-site.com',  
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'facebook', // Usually your GitHub org/user name.
  projectName: 'BifrostDesignSystemDocs', // Usually your repo name.
  customFields: {
    bifrostTokensRepository: {
      baseUrl: "https://bitbucket.org/bifrostdesignsystem/bifrosttokens/src/master",
    }
  },
  stylesheets: [
    {
      href: 'node_modules/@bifrost-ds/bifrosttokens/build/web/css/variables.css',
      type: 'text/css',
    },
  ],
  plugins: [
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'components',
        path: 'components',
        routeBasePath: 'components',
        editCurrentVersion: true,
        sidebarPath: require.resolve('./sidebarsComponents.js'),
        showLastUpdateAuthor: true,
        showLastUpdateTime: true,
      },       
    ],
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'tokens',
        path: 'tokens',
        routeBasePath: 'tokens',
        editCurrentVersion: true,
        sidebarPath: require.resolve('./sidebarsTokens.js'),
        showLastUpdateAuthor: true,
        showLastUpdateTime: true,
      },       
    ],
  ],
  themeConfig: {
    colorMode: {
      defaultMode: 'light',
      disableSwitch: false,
      respectPrefersColorScheme: true,
      switchConfig: {
        darkIcon: '\u263E',
        lightIcon: '\u263C',
        // React inline style object
        // see https://reactjs.org/docs/dom-elements.html#style
        darkIconStyle: {
          marginLeft: '2px',
          color: 'white'
        },
        lightIconStyle: {
          marginLeft: '1px',
          color: 'white'
        },
      },
    },
    navbar: {
      title: '',
      logo: {
        alt: 'My Site Logo',
        src: 'img/Logo_on_light.svg',
        srcDark: 'img/Logo_on_dark.svg', // Dark mode logo
      },
      items: [
        {
          type: 'doc',
          docId: 'intro',
          position: 'left',
          label: 'Docs',
        },
        {
          to: '/components/intro',
          label: 'Components',
          position: 'left',
          activeBaseRegex: `/components/`,
        },
        {
          to: '/tokens/intro',
          label: 'Tokens',
          position: 'left',
          activeBaseRegex: `/tokens/`,
        },
        {to: '/blog', label: 'News', position: 'left'},
        {
          href: 'https://bitbucket.org/bifrostdesignsystem/bifrostdesignsystemdocs',
          position: 'right',
          className: 'header-bitbucket-link',
          'aria-label': 'Bitbucket repository',
        },
      ],
      hideOnScroll: true, // Hides nav bar on scroll
    },
    
    footer: {
      logo: {
        alt: 'bifrost logo',
        src: 'img/Logo_3.svg',
      },
      copyright: `Copyright © ${new Date().getFullYear()} Bifrost-DS-Docs, Inc. Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://bitbucket.org/bifrostdesignsystem/bifrostdesignsystemdocs/src/master/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://bitbucket.org/bifrostdesignsystem/bifrostdesignsystemdocs/src/master/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
