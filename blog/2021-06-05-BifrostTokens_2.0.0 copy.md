---
slug: Bifröst Tokens 2.0.0 Release Notes
title:  Bifröst Tokens 2.0.0 Release Notes
author: Magnus V. Jensen
author_title: CEO @ Bifröst-ds
author_url: https://magnusvjensen.com/
author_image_url: https://avatars.githubusercontent.com/u/36401508?v=4
tags: [Bifröst Tokens, Release, Breaking Changes]
---

## Release Notes Bifröst Tokens v2.0.0

:::caution

Bifröst Tokens 2.0.0 has just been released. The upgrade introduces breaking changes!

:::

### List of changes

- adding "bt" prefix to all tokens inkls json formats
- Changing style from snake to kebab
    - bt_color_primary_indigo_100(old) -> bt-color-primary-prim-a-100(new)


:::info

See Design Tokens list for new names

:::

