---
slug: Bifröst Tokens 1.0.0 Release Notes
title:  Bifröst Tokens 1.0.0 Release Notes
author: Magnus V. Jensen
author_title: CEO @ Bifröst-ds
author_url: https://magnusvjensen.com/
author_image_url: https://avatars.githubusercontent.com/u/36401508?v=4
tags: [Bifröst Tokens, Release, Breaking Changes]
---

## Release Notes Bifröst Tokens v1.0.0

:::caution

Bifröst Tokens 1.0.0 has just been released. The upgrade introduces breaking changes!

:::

### List of changes

- Alias naming conventions has changed from fx 
making room for future implementaions of prim_b, prim_c primary color variations
    - bt_color_primary_indigo_100(old) -> bt_color_primary_prim_a_100(new)
    - bt_color_neutrals_silver_100(old) -> bt_color_neutrals_grey_b_100(new)
    - bt_color_secondary_red_100(old) -> bt_color_secondary_red_a_100(new)
- Bug Fix!: Missing _120 value on all alias tokens has been fixed

:::info

See Design Tokens list for new names

:::

