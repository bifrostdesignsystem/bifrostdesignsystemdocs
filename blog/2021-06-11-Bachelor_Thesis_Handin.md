---
slug: Bachelor Thesis Handin
title:  Bachelor Thesis Handin
author: Magnus V. Jensen
author_title: CEO @ Bifröst-ds
author_url: https://magnusvjensen.com/
author_image_url: https://avatars.githubusercontent.com/u/36401508?v=4
tags: [Bachelor Thesis Handin]
---

## The Bachelor has been handed in
:::success

The Bachelors thesis is finally finished and has been handed in. I feel relieved.

:::



